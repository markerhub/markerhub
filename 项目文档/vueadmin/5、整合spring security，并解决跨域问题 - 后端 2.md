# 更多项目学习，请查看这里 -> [https://gitee.com/markerhub/markerhub](https://gitee.com/markerhub/markerhub)
# 更多项目学习，请查看这里 -> [https://gitee.com/markerhub/markerhub](https://gitee.com/markerhub/markerhub)

---

### 6. 整合Spring Security
很多人不懂spring security，觉得这个框架比shiro要难，的确，security更加复杂一点，同时功能也更加强大，我们首先来看一下security的原理，这里我们引用一张来自江南一点雨大佬画的一张原理图（[https://blog.csdn.net/u012702547/article/details/89629415](https://blog.csdn.net/u012702547/article/details/89629415)）：

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/a369abde58ad4ea48d59f7a9c4b64dd7.png)

（引自江南一点雨的博客）

上面这张图一定要好好看，特别清晰，毕竟security是责任链的设计模式，是一堆过滤器链的组合，如果对于这个流程都不清楚，那么你就谈不上理解security。那么针对我们现在的这个系统，我们可以自己设计一个security的认证方案，结合江南一点雨大佬的博客，我们得到这样一套流程：

[https://www.processon.com/view/link/606b0b5307912932d09adcb3](https://www.processon.com/view/link/606b0b5307912932d09adcb3)

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/d3a13b91b990440288a5abf2498d5da5.png)


流程说明：

1. 客户端发起一个请求，进入 Security 过滤器链。
2. 当到 LogoutFilter 的时候判断是否是登出路径，如果是登出路径则到 logoutHandler ，如果登出成功则到 logoutSuccessHandler 登出成功处理。如果不是登出路径则直接进入下一个过滤器。
3. 当到 UsernamePasswordAuthenticationFilter 的时候判断是否为登录路径，如果是，则进入该过滤器进行登录操作，如果登录失败则到 AuthenticationFailureHandler ，登录失败处理器处理，如果登录成功则到 AuthenticationSuccessHandler 登录成功处理器处理，如果不是登录请求则不进入该过滤器。
4. 进入认证BasicAuthenticationFilter进行用户认证，成功的话会把认证了的结果写入到SecurityContextHolder中SecurityContext的属性authentication上面。如果认证失败就会交给AuthenticationEntryPoint认证失败处理类，或者抛出异常被后续ExceptionTranslationFilter过滤器处理异常，如果是AuthenticationException就交给AuthenticationEntryPoint处理，如果是AccessDeniedException异常则交给AccessDeniedHandler处理。
5. 当到 FilterSecurityInterceptor 的时候会拿到 uri ，根据 uri 去找对应的鉴权管理器，鉴权管理器做鉴权工作，鉴权成功则到 Controller 层，否则到 AccessDeniedHandler 鉴权失败处理器处理。
   Spring Security 实战干货：必须掌握的一些内置 Filter：[https://blog.csdn.net/qq_35067322/article/details/102690579](https://blog.csdn.net/qq_35067322/article/details/102690579)

ok，上面我们说的流程中涉及到几个组件，有些是我们需要根据实际情况来重写的。因为我们是使用json数据进行前后端数据交互，并且我们返回结果也是特定封装的。我们先再总结一下我们需要了解的几个组件：

* LogoutFilter  - 登出过滤器
* logoutSuccessHandler - 登出成功之后的操作类
* UsernamePasswordAuthenticationFilter  - from提交用户名密码登录认证过滤器
* AuthenticationFailureHandler  - 登录失败操作类
* AuthenticationSuccessHandler  - 登录成功操作类
* BasicAuthenticationFilter - Basic身份认证过滤器
* SecurityContextHolder - 安全上下文静态工具类
* AuthenticationEntryPoint - 认证失败入口
* ExceptionTranslationFilter - 异常处理过滤器
* AccessDeniedHandler - 权限不足操作类
* FilterSecurityInterceptor - 权限判断拦截器、出口
  有了上面的组件，那么认证与授权两个问题我们就已经接近啦，我们现在需要做的就是去重写我们的一些关键类。

#### 引入Security与jwt

首先我们导入security包，因为我们前后端交互用户凭证用的是JWT，所以我们也导入jwt的相关包，然后因为验证码的存储需要用到redis，所以引入redis。最后为了一些工具类，我们引入hutool。

* pom.xml
```plain
<!-- springboot security -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
<!-- jwt -->
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt</artifactId>
    <version>0.9.1</version>
</dependency>
<dependency>
    <groupId>com.github.axet</groupId>
    <artifactId>kaptcha</artifactId>
    <version>0.0.9</version>
</dependency>
<!-- hutool工具类-->
<dependency>
    <groupId>cn.hutool</groupId>
    <artifactId>hutool-all</artifactId>
    <version>5.3.3</version>
</dependency>
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-lang3</artifactId>
    <version>3.11</version>
</dependency>
```
启动redis，然后我们再启动项目，这时候我们再去访问[http://localhost:8081/test](http://localhost:8081/test)，会发现系统会先判断到你未登录跳转到[http://localhost:8081/login](http://localhost:8081/login)，因为security内置了登录页，用户名为user，密码在启动项目的时候打印在了控制台。登录完成之后我们才可以正常访问接口。
因为每次启动密码都会改变，所以我们通过配置文件来配置一下默认的用户名和密码：

* application.yml
```plain
spring:
  security:
    user:
      name: user
      password: 111111
```

#### 用户认证

首先我们来解决用户认证问题，分为首次登陆，和二次认证。

* 首次登录认证：用户名、密码和验证码完成登录
* 二次token认证：请求头携带Jwt进行身份认证
  使用用户名密码来登录的，然后我们还想添加图片验证码，那么security给我们提供的UsernamePasswordAuthenticationFilter能使用吗？

首先security的所有过滤器都是没有图片验证码这回事的，看起来不适用了。其实这里我们可以灵活点，如果你依然想沿用自带的UsernamePasswordAuthenticationFilter，那么我们就在这过滤器之前添加一个图片验证码过滤器。当然了我们也可以通过自定义过滤器继承UsernamePasswordAuthenticationFilter，然后自己把验证码验证逻辑和认证逻辑写在一起，这也是一种解决方式。

我们这次解决方式是在UsernamePasswordAuthenticationFilter之前自定义一个图片过滤器CaptchaFilter，提前校验验证码是否正确，这样我们就可以使用UsernamePasswordAuthenticationFilter了，然后登录正常或失败我们都可以通过对应的Handler来返回我们特定格式的封装结果数据。

#### 生成验证码

首先我们先生成验证码，之前我们已经引用了google的验证码生成器，我们先来配置一下图片验证码的生成规则：

* com.markerhub.config.KaptchaConfig
```plain
@Configuration
public class KaptchaConfig {
   @Bean
   public DefaultKaptcha producer() {
      Properties properties = new Properties();
      properties.put("kaptcha.border", "no");
      properties.put("kaptcha.textproducer.font.color", "black");
      properties.put("kaptcha.textproducer.char.space", "4");
      properties.put("kaptcha.image.height", "40");
      properties.put("kaptcha.image.width", "120");
      properties.put("kaptcha.textproducer.font.size", "30");
      Config config = new Config(properties);
      DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
      defaultKaptcha.setConfig(config);
      return defaultKaptcha;
   }
}
```
上面我定义了图片验证码的长宽字体颜色等，自己可以调整哈。
然后我们通过控制器提供生成验证码的方法：

* com.markerhub.controller.AuthController
```plain
@Slf4j
@RestController
public class AuthController extends BaseController{
   @Autowired
   private Producer producer;
   /**
    * 图片验证码
    */
   @GetMapping("/captcha")
   public Result captcha(HttpServletRequest request, HttpServletResponse response) throws IOException {
      String code = producer.createText();
      String key = UUID.randomUUID().toString();

      BufferedImage image = producer.createImage(code);
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      ImageIO.write(image, "jpg", outputStream);
      BASE64Encoder encoder = new BASE64Encoder();
      String str = "";
      String base64Img = str + encoder.encode(outputStream.toByteArray());
      
      // 存储到redis中
      redisUtil.hset(Const.captcha_KEY, key, code, 120);
      log.info("验证码 -- {} - {}", key, code);
      return Result.succ(
            MapUtil.builder()
            .put("token", key)
            .put("base64Img", base64Img)
            .build()
      );
   }
}
```
因为前后端分离，我们禁用了session，所以我们把验证码放在了redis中，使用一个随机字符串作为key，并传送到前端，前端再把随机字符串和用户输入的验证码提交上来，这样我们就可以通过随机字符串获取到保存的验证码和用户的验证码进行比较了是否正确了。
然后因为图片验证码的方式，所以我们进行了encode，把图片进行了base64编码，这样前端就可以显示图片了。

而前端的处理，我们之前是使用了mockjs进行随机生成数据的，现在后端有接口之后，我们只需要在main.js中去掉mockjs的引入即可，这样前端就可以访问后端的接口而不被mock拦截了。

#### 验证码认证过滤器

图片验证码进行认证验证码是否正确。

* CaptchaFilter
```plain
/**
 * 图片验证码校验过滤器，在登录过滤器前
 */
@Slf4j
@Component
public class CaptchaFilter extends OncePerRequestFilter {
   private final String loginUrl = "/login";
   @Autowired
   RedisUtil redisUtil;
   @Autowired
   LoginFailureHandler loginFailureHandler;
   @Override
   protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
         throws ServletException, IOException {
      String url = request.getRequestURI();
      if (loginUrl.equals(url) && request.getMethod().equals("POST")) {
         log.info("获取到login链接，正在校验验证码 -- " + url);
         try {
            validate(request);
         } catch (CaptchaException e) {
            log.info(e.getMessage());
            // 交给登录失败处理器处理
            loginFailureHandler.onAuthenticationFailure(request, response, e);
         }
      }
      filterChain.doFilter(request, response);
   }
   private void validate(HttpServletRequest request) {
      String code = request.getParameter("code");
      String token = request.getParameter("token");
      if (StringUtils.isBlank(code) || StringUtils.isBlank(token)) {
         throw new CaptchaException("验证码不能为空");
      }
      if(!code.equals(redisUtil.hget(Const.captcha_KEY, token))) {
         throw new CaptchaException("验证码不正确");
      }
      // 一次性使用
      redisUtil.hdel(Const.captcha_KEY, token);
   }
}
```
上面代码中，因为验证码需要存储，所以添加了RedisUtil工具类，这个工具类代码我们就不贴出来了。
* com.markerhub.util.RedisUtil
  然后验证码出错的时候我们返回异常信息，这是一个认证异常，所以我们自定了一个CaptchaException：

* com.javacat.common.exception.CaptchaException
```plain
public class CaptchaException extends AuthenticationException {
   public CaptchaException(String msg) {
      super(msg);
   }
}
```
* com.markerhub.common.lang.Const
```plain
public class Const {
   public static final String captcha_KEY = "captcha";
}
```
然后认证失败的话，我们之前说过，登录失败的时候交给AuthenticationFailureHandler，所以我们自定义了LoginFailureHandler
* com.markerhub.security.LoginFailureHandler
```plain
@Component
public class LoginFailureHandler implements AuthenticationFailureHandler {
   @Override
   public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
      response.setContentType("application/json;charset=UTF-8");
      ServletOutputStream outputStream = response.getOutputStream();
      Result result = Result.fail(
            "Bad credentials".equals(exception.getMessage()) ? "用户名或密码不正确" : exception.getMessage()
      );
      outputStream.write(JSONUtil.toJsonStr(result).getBytes("UTF-8"));
      outputStream.flush();
      outputStream.close();
   }
}
```
其实主要就是获取异常的消息，然后封装到Result，最后转成json返回给前端而已哈。
然后我们配置SecurityConfig

* com.markerhub.config.SecurityConfig
```plain
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

   @Autowired
   LoginFailureHandler loginFailureHandler;
   
   @Autowired
   CaptchaFilter captchaFilter;
   
   public static final String[] URL_WHITELIST = {

         "/webjars/**",
         "/favicon.ico",
         
         "/captcha",
         "/login",
         "/logout",
   };
   
   @Override
   protected void configure(HttpSecurity http) throws Exception {
      http.cors().and().csrf().disable()
            .formLogin()
            .failureHandler(loginFailureHandler)
            
            .and()
            .authorizeRequests()
            .antMatchers(URL_WHITELIST).permitAll() //白名单
            .anyRequest().authenticated()
            // 不会创建 session
            .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            
            .and()
            .addFilterBefore(captchaFilter, UsernamePasswordAuthenticationFilter.class) // 登录验证码校验过滤器
      ;
   }
}
```
首先formLogin我们定义了表单登录提交的方式以及定义了登录失败的处理器，后面我们还要定义登录成功的处理器的。然后authorizeRequests我们除了白名单的链接之外其他请求都会被拦截。再然后就是禁用session，最后是设定验证码过滤器在登录过滤器之前。
然后我们打开前端的/login，发现出现了跨域的问题，后面我处理，我们先用postman调试接口。

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/1315f4f63f7a4bb78f565216fb4a3056.png)

可以看到，我们的随机码token和base64Img编码都是正常的。控制台上看到我们的验证是2yyxm：

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/2115560e24cc4849ac9a78266c92e850.png)

然后我们尝试登录，因为之前我们已经设置了用户名密码为user/111111，所以我们提交表单的时候再带上我们的token和验证码。

这时候我们就可以去提交表单了吗，其实还不可以，为啥？因为就算我们登录成功，security默认跳转到/链接，但是又会因为没有权限访问/，所有又会教你去登录，所以我们必须取消原先默认的登录成功之后的操作，根据我们之前分析的流程，登录成功之后会走AuthenticationSuccessHandler，因此在登录之前，我们先去自定义这个登录成功操作类：

* com.markerhub.security.LoginSuccessHandler
```plain
@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {
   @Autowired
   JwtUtils jwtUtils;
   
   @Override
   public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
      response.setContentType("application/json;charset=UTF-8");
      ServletOutputStream outputStream = response.getOutputStream();
      
      // 生成jwt返回
      String jwt = jwtUtils.generateToken(authentication.getName());
      response.setHeader(jwtUtils.getHeader(), jwt);
      
      Result result = Result.succ("");
      outputStream.write(JSONUtil.toJsonStr(result).getBytes("UTF-8"));
      outputStream.flush();
      outputStream.close();
   }
}
```
登录成功之后我们利用用户名生成jwt，jwtUtils这个工具类我就不贴代码了哈，去看我们项目源码，然后把jwt作为请求头返回回去，名称就叫Authorization哈。我们需要在配置文件中配置一些jwt的一些密钥信息：
* application.yml
```plain
markerhub:
  jwt:
    # 加密秘钥
    secret: f4e2e52034348f86b67cde581c0f9eb5
    # token有效时长，7天，单位秒
    expire: 604800
    header: Authorization
```
然后我们再security配置中添加上登录成功之后的操作类：
* com.markerhub.config.SecurityConfig
```plain
@Autowired
LoginSuccessHandler loginSuccessHandler;

...
# configure代码：

http.cors().and().csrf().disable()
      .formLogin()
      .failureHandler(loginFailureHandler)
      .successHandler(loginSuccessHandler)
```
然后我们去postman的进行我们的登录测试：
![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/423adf281da7466fba9dd18b58232b78.png)

上面我们可以看到，我们已经可以登录成功了。然后去结果的请求头中查看jwt：

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/a50436dca00d49ff9b257436f5d6e336.png)

搞定，登录成功啦，验证码也正常验证了。

#### 身份认证 - 1

登录成功之后前端就可以获取到了jwt的信息，前端中我们是保存在了store中，同时也保存在了localStorage中，然后每次axios请求之前，我们都会添加上我们的请求头信息，可以回顾一下：

* 前端项目的axios.js
  ![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/d51895b59c4344e7909dd79b9bc1da3f.png)


所以后端进行用户身份识别的时候，我们需要通过请求头中获取jwt，然后解析出我们的用户名，这样我们就可以知道是谁在访问我们的接口啦，然后判断用户是否有权限等操作。

那么我们自定义一个过滤器用来进行识别jwt。

* JWTAuthenticationFilter
```plain
@Slf4j
public class JWTAuthenticationFilter extends BasicAuthenticationFilter {
   @Autowired
   JwtUtils jwtUtils;
   @Autowired
   RedisUtil redisUtil;
   @Autowired
   SysUserService sysUserService;
   public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
      super(authenticationManager);
   }
   @Override
   protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
      log.info("jwt 校验 filter");
      String jwt = request.getHeader(jwtUtils.getHeader());
      if (StrUtil.isBlankOrUndefined(jwt)) {
         chain.doFilter(request, response);
         return;
      }
      Claims claim = jwtUtils.getClaimByToken(jwt);
      if (claim == null) {
         throw new JwtException("token异常！");
      }
      if (jwtUtils.isTokenExpired(claim.getExpiration())) {
         throw new JwtException("token已过期");
      }
      String username = claim.getSubject();
      log.info("用户-{}，正在登陆！", username);
      UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
            = new UsernamePasswordAuthenticationToken(username, null, new TreeSet<>());
      SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
      chain.doFilter(request, response);
   }
}
```
上面的逻辑也很简单，正如我前面说到的，获取到用户名之后我们直接把封装成UsernamePasswordAuthenticationToken，之后交给SecurityContextHolder参数传递authentication对象，这样后续security就能获取到当前登录的用户信息了，也就完成了用户认证。
当认证失败的时候会进入AuthenticationEntryPoint，于是我们自定义认证失败返回的数据：

* com.markerhub.security.JwtAuthenticationEntryPoint
```plain
/**
 * 定义认证失败处理类
 */
@Slf4j
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
   @Override
   public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
         throws IOException {
      log.info("认证失败！未登录！");
      response.setContentType("application/json;charset=UTF-8");
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      ServletOutputStream outputStream = response.getOutputStream();
      
      Result result = Result.fail("请先登录！");
      outputStream.write(JSONUtil.toJsonStr(result).getBytes("UTF-8"));
      outputStream.flush();
      outputStream.close();
   }
}
```
不过是啥原因，认证失败，我们就要求重新登录，所以返回的信息直接明了“请先登录！”哈哈。
然后我们把认证过滤器和认证失败入口配置到SecurityConfig中：

* com.markerhub.config.SecurityConfig
```plain
@Bean
JWTAuthenticationFilter jwtAuthenticationFilter() throws Exception {
   JWTAuthenticationFilter filter = new JWTAuthenticationFilter(authenticationManager());
   return filter;
}

.and()
.exceptionHandling()
.authenticationEntryPoint(jwtAuthenticationEntryPoint)

.and()
.addFilter(jwtAuthenticationFilter())
.addFilterBefore(captchaFilter, UsernamePasswordAuthenticationFilter.class) // 登录验证码校验过滤器
```
这样携带jwt请求头我们就可以正常访问我们的接口了。
#### 身份认证 - 2

之前我们的用户名密码配置在配置文件中的，而且密码也用的是明文，这明显不符合我们的要求，我们的用户必须是存储在数据库中，密码也是得经过加密的。所以我们先来解决这个问题，然后再去弄授权。

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/13aba0157fca4591bc1f74311850a5b0.png)



首先来插入一条用户数据，但这里有个问题，就是我们的密码怎么生成？密码怎么来的？这里我们使用Security内置了的BCryptPasswordEncoder，里面就有生成和匹配密码是否正确的方法，也就是加密和验证策略。因此我们再SecurityConfig中进行配置：

* com.markerhub.config.SecurityConfig
```plain
@Bean
BCryptPasswordEncoder bCryptPasswordEncoder() {
   return new BCryptPasswordEncoder();
}
```
这样系统就会使用我们找个新的密码策略进行匹配密码是否正常了。之前我们配置文件配置的用户名密码去掉：
* application.yml
```plain
#  security:
#    user:
#      name: user
#      password: 111111
```
ok，我们先使用BCryptPasswordEncoder给我们生成一个密码，给数据库添加一条数据先，我们再TestController中注入BCryptPasswordEncoder，然后使用encode进行密码加密，对了，记得在SecurityConfig中吧/test/**添加白名单哈，不然访问会提示你登录！！
* com.markerhub.controller.TestController
```plain
@Autowired
BCryptPasswordEncoder bCryptPasswordEncoder;

@GetMapping("/test/pass")
public Result passEncode() {
   // 密码加密
   String pass = bCryptPasswordEncoder.encode("111111");
   
   // 密码验证
   boolean matches = bCryptPasswordEncoder.matches("111111", pass);
   
   return Result.succ(MapUtil.builder()
         .put("pass", pass)
         .put("marches", matches)
         .build()
   );
}
```
可以看到我密码是111111，加密以及验证的结果如下：$2a$10$R7zegeWzOXPw871CmNuJ6upC0v8D373GuLuTw8jn6NET4BkPRZfgK
![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/8faee44b6f104f1ba0f49721d0cdef17.png)


data中的那一串字符串就是我们的密码了，可以看到marches也是true，说明密码验证也是正确的，我们添加到我们数据库sys_user表中：

```plain
INSERT INTO `vueadmin`.`sys_user` (`id`, `username`, `password`, `avatar`, `email`, `city`, `created`, `updated`, `last_login`, `statu`) VALUES ('1', 'admin', '$2a$10$R7zegeWzOXPw871CmNuJ6upC0v8D373GuLuTw8jn6NET4BkPRZfgK', 'https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/5a9f48118166308daba8b6da7e466aab.jpg', '123@qq.com', '广州', '2021-01-12 22:13:53', '2021-01-16 16:57:32', '2020-12-30 08:38:37', '1');
```
后面我们就可以使用admin/111111登录我们的系统哈。
但是先我们登录过程系统不是从我们数据库中获取数据的，因此，我们需要重新定义这个查用户数据的过程，我们需要重写UserDetailsService接口。

* com.markerhub.security.UserDetailsServiceImpl
```plain
@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

   @Autowired
   SysUserService sysUserService;
   
   @Override
   public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
      SysUser sysUser = sysUserService.getByUsername(username);
      if (sysUser == null) {
         throw new UsernameNotFoundException("用户名或密码不正确!");
      }
      return new AccountUser(sysUser.getId(), sysUser.getUsername(), sysUser.getPassword(), new TreeSet<>());
   }
}
```
因为security在认证用户身份的时候会调用UserDetailsService.loadUserByUsername()方法，因此我们重写了之后security就可以根据我们的流程去查库获取用户了。然后我们把UserDetailsServiceImpl配置到SecurityConfig中：
* com.markerhub.config.SecurityConfig
```plain
@Autowired
UserDetailsServiceImpl userDetailsService;

@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
   auth.userDetailsService(userDetailsService);
}
```
然后上面UserDetailsService.loadUserByUsername()默认返回的UserDetails，我们自定义了AccountUser去重写了UserDetails，这也是为了后面我们可能会调整用户的一些数据等。
* com.markerhub.security.AccountUser
```plain
public class AccountUser implements UserDetails {
   private Long userId;
   private String password;
   private final String username;
   private final Collection<? extends GrantedAuthority> authorities;
   private final boolean accountNonExpired;
   private final boolean accountNonLocked;
   private final boolean credentialsNonExpired;
   private final boolean enabled;
   public AccountUser(Long userId, String username, String password, Collection<? extends GrantedAuthority> authorities) {
      this(userId, username, password, true, true, true, true, authorities);
   }
   public AccountUser(Long userId, String username, String password, boolean enabled,
                      boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,
                      Collection<? extends GrantedAuthority> authorities) {
      Assert.isTrue(username != null && !"".equals(username) && password != null, "Cannot pass null or empty values to constructor");
      this.userId = userId;
      this.username = username;
      this.password = password;
      this.enabled = enabled;
      this.accountNonExpired = accountNonExpired;
      this.credentialsNonExpired = credentialsNonExpired;
      this.accountNonLocked = accountNonLocked;
      this.authorities = authorities;
   }
   public Long getUserId() {
      return userId;
   }
   ...  
}
```
其实数据基本没变，我就添加多了一个用户的id而已。
ok，万事俱备，我们再次尝试去登录，看能不能登录成功。

1、获取验证码：

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/ff679f096653440c9168c1024dc8f7e8.png)

2、从控制台获取到对应的验证码

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/42b7937f0a6b4f789a1cff481040e37f.png)

3、提交登录表单

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20221031/f22bba3cac9845698e8f8599eb681335.png)


4、登录成功，并在请求头中获取到了Authorization，也就是JWT。完美！！

#### 解决授权

然后关于权限部分，也是security的重要功能，当用户认证成功之后，我们就知道谁在访问系统接口，这是又有一个问题，就是这个用户有没有权限来访问我们这个接口呢，要解决这个问题，我们需要知道用户有哪些权限，哪些角色，这样security才能我们做权限判断。

之前我们已经定义及几张表，用户、角色、菜单、以及一些关联表，一般当权限粒度比较细的时候，我们都通过判断用户有没有此菜单或操作的权限，而不是通过角色判断，而用户和菜单是不直接做关联的，是通过用户拥有哪些角色，然后角色拥有哪些菜单权限这样来获得的。

问题1：我们是在哪里赋予用户权限的？有两个地方：

* 1、用户登录，调用调用UserDetailsService.loadUserByUsername()方法时候可以返回用户的权限信息。
* 2、接口调用进行身份认证过滤器时候JWTAuthenticationFilter，需要返回用户权限信息
  问题2：在哪里决定什么接口需要什么权限？

Security内置的权限注解：

* @PreAuthorize：方法执行前进行权限检查
* @PostAuthorize：方法执行后进行权限检查
* @Secured：类似于 @PreAuthorize
  可以在Controller的方法前添加这些注解表示接口需要什么权限。

比如需要Admin角色权限：

```plain
@PreAuthorize("hasRole('admin')")
```
比如需要添加管理员的操作权限
```plain
@PreAuthorize("hasAuthority('sys:user:save')")
```

ok，我们再来整体梳理一下授权、验证权限的流程：

1. 用户登录或者调用接口时候识别到用户，并获取到用户的权限信息
2. 注解标识Controller中的方法需要的权限或角色
3. Security通过FilterSecurityInterceptor匹配URI和权限是否匹配
4. 有权限则可以访问接口，当无权限的时候返回异常交给AccessDeniedHandler操作类处理
   ok，流程清晰之后我们就开始我们的编码：

* UserDetailsServiceImpl
```plain
@Override
public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

   ...   

   return new AccountUser(sysUser.getId(), sysUser.getUsername(), sysUser.getPassword(), getUserAuthority(sysUser.getId()));
}

public List<GrantedAuthority> getUserAuthority(Long userId) {
   // 通过内置的工具类，把权限字符串封装成GrantedAuthority列表
   return  AuthorityUtils.commaSeparatedStringToAuthorityList(
         sysUserService.getUserAuthorityInfo(userId)
   );
}
```
* com.markerhub.security.JWTAuthenticationFilter
```plain
SysUser sysUser = sysUserService.getByUsername(username);
List<GrantedAuthority> grantedAuthorities = userDetailsService.getUserAuthority(sysUser.getId());
UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
      = new UsernamePasswordAuthenticationToken(username, null, grantedAuthorities);

```
代码中的com.markerhub.service.impl.SysUserServiceImpl#getUserAuthorityInfo是重点：
```plain
@Slf4j
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

   ... 

   @Override
   public String getUserAuthorityInfo(Long userId) {
      SysUser sysUser = this.getById(userId);
      String authority = null;
      
      if (redisUtil.hasKey("GrantedAuthority:" + sysUser.getUsername())) {
         // 优先从缓存获取
         authority = (String)redisUtil.get("GrantedAuthority:" + sysUser.getUsername());
         
      } else {
      
         List<SysRole> roles = sysRoleService.list(new QueryWrapper<SysRole>()
               .inSql("id", "select role_id from sys_user_role where user_id = " + userId));
         List<Long> menuIds = sysUserMapper.getNavMenuIds(userId);
         List<SysMenu> menus = sysMenuService.listByIds(menuIds);
         
         String roleNames = roles.stream().map(r -> "ROLE_" + r.getCode()).collect(Collectors.joining(","));
         String permNames = menus.stream().map(m -> m.getPerms()).collect(Collectors.joining(","));
         
         authority = roleNames.concat(",").concat(permNames);
         log.info("用户ID - {} ---拥有的权限：{}", userId, authority);
         
         redisUtil.set("GrantedAuthority:" + sysUser.getUsername(), authority, 60*60);
         
      }
      return authority;
   }
}
```
可以看到，我通过用户id分别获取到用户的角色信息和菜单信息，然后通过逗号链接起来，因为角色信息我们需要这样“ROLE_”+角色，所以才有了上面的写法：
比如用户拥有Admin角色和添加用户权限，则最后的字符串是：**ROLE_admin,sys:user:save**。

同时为了避免多次查库，我做了一层缓存，这里理解应该不难。

然后sysUserMapper.getNavMenuIds(userId)因为要查询数据库，具体SQL如下：

* com.markerhub.mapper.SysUserMapper#getNavMenuIds
```plain
<select id="getNavMenuIds" resultType="java.lang.Long">
    SELECT
        DISTINCT rm.menu_id
    FROM
        sys_user_role ur
    LEFT JOIN `sys_role_menu` rm ON rm.role_id = ur.role_id
    WHERE
        ur.user_id = #{userId};
</select>
```
上面表示通过用户ID获取用户关联的菜单的id，因此需要用到两个中间表的关联了。
ok，这样我们就赋予了用户角色和操作权限了。后面我们只需要在Controller添加上具体注解表示需要的权限，Security就会自动帮我们自动完成权限校验了。

#### 权限缓存

因为上面我在获取用户权限那里添加了个缓存，这时候问题来了，就是权限缓存的实时更新问题，比如当后台更新某个管理员的权限角色信息的时候如果权限缓存信息没有实时更新，就会出现操作无效的问题，那么我们现在点定义几个方法，用于清除某个用户或角色或者某个菜单的权限的方法：

* com.markerhub.service.impl.SysUserServiceImpl
```plain
// 删除某个用户的权限信息
@Override
public void clearUserAuthorityInfo(String username) {
   redisUtil.del("GrantedAuthority:" + username);
}

// 删除所有与该角色关联的用户的权限信息
@Override
public void clearUserAuthorityInfoByRoleId(Long roleId) {
   List<SysUser> sysUsers = this.list(new QueryWrapper<SysUser>()
         .inSql("id", "select user_id from sys_user_role where role_id = " + roleId)
   );
   sysUsers.forEach(u -> {
      this.clearUserAuthorityInfo(u.getUsername());
   });
}

// 删除所有与该菜单关联的所有用户的权限信息
@Override
public void clearUserAuthorityInfoByMenuId(Long menuId) {
   List<SysUser> sysUsers = sysUserMapper.listByMenuId(menuId);
   sysUsers.forEach(u -> {
      this.clearUserAuthorityInfo(u.getUsername());
   });
}
```
上面最后一个方法查到了与菜单关联的所有用户的，具体sql如下：
* com.markerhub.mapper.SysUserMapper#listByMenuId
```plain
<select id="listByMenuId" resultType="com.javacat.entity.SysUser">
    SELECT
    DISTINCT
        su.*
    FROM
        sys_user_role ur
    LEFT JOIN `sys_role_menu` rm ON rm.role_id = ur.role_id
    LEFT JOIN `sys_user` su ON su.id = ur.user_id
    WHERE
        rm.menu_id = #{menuId};
</select>
```
有了这几个方法之后，在哪里调用？这就简单了，在更新、删除角色权限、更新、删除菜单的时候调用，虽然我们现在还没写到这几个方法，后续我们再写增删改查的时候记得加上就行啦。
#### 退出数据返回

jwt -username

token - 随机码 - redis

* com.markerhub.security.JwtLogoutSuccessHandler
```plain
@Component
public class JwtLogoutSuccessHandler implements LogoutSuccessHandler {
   @Autowired
   JwtUtils jwtUtils;
   @Override
   public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
         throws IOException, ServletException {
      if (authentication != null) {
         new SecurityContextLogoutHandler().logout(request, response, authentication);
      }
      response.setContentType("application/json;charset=UTF-8");
      response.setHeader(jwtUtils.getHeader(), "");
      ServletOutputStream out = response.getOutputStream();
      Result result = Result.succ("");
      out.write(JSONUtil.toJsonStr(result).getBytes("UTF-8"));
      out.flush();
      out.close();
   }

```

#### 无权限数据返回

* com.markerhub.security.JwtAccessDeniedHandler
```plain
@Slf4j
@Component
public class JwtAccessDeniedHandler implements AccessDeniedHandler {
   @Override
   public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
         throws IOException, ServletException {
//    response.sendError(HttpServletResponse.SC_FORBIDDEN, accessDeniedException.getMessage());
      log.info("权限不够！！");
      response.setContentType("application/json;charset=UTF-8");
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
      ServletOutputStream outputStream = response.getOutputStream();
      Result result = Result.fail(accessDeniedException.getMessage());
      outputStream.write(JSONUtil.toJsonStr(result).getBytes("UTF-8"));
      outputStream.flush();
      outputStream.close();
   }
}
```

致此，SpringSecurity就已经完美整合到了我们的项目中来了。

### 7. 解决跨域问题
上面的调试我们都是使用的postman，如果我们和前端进行对接的时候，会出现跨域的问题，如何解决？

* com.markerhub.config.CorsConfig
```plain
@Configuration
public class CorsConfig implements WebMvcConfigurer {

   private CorsConfiguration buildConfig() {
      CorsConfiguration corsConfiguration = new CorsConfiguration();
      corsConfiguration.addAllowedOrigin("*");
      corsConfiguration.addAllowedHeader("*");
      corsConfiguration.addAllowedMethod("*");
      corsConfiguration.addExposedHeader("Authorization");
      return corsConfiguration;
   }
   
   @Bean
   public CorsFilter corsFilter() {
      UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
      source.registerCorsConfiguration("/**", buildConfig());
      return new CorsFilter(source);
   }
   
   @Override
   public void addCorsMappings(CorsRegistry registry) {
      registry.addMapping("/**")
            .allowedOrigins("*")
//          .allowCredentials(true)
            .allowedMethods("GET", "POST", "DELETE", "PUT")
            .maxAge(3600);
   }
}
```

### 8. 前后端对接的问题

因为我们之前开发前端的时候，我们都是使用mockjs返回随机数据的，一般来说问题不会很大，我就怕有些同学再去掉mock之后，和后端对接却显示不出数据，这就尴尬了。这时候我建议你去看我的开发视频哈。

后面因为都是接口的增删改查，难度其实不是特别大，所以大部分时候我都会直接贴代码，如果想看手把手教程，还是去看我的教学视频哈，B站搜索MarkerHub就可以啦，公众号也是叫MarkerHub。
