# 更多项目学习，请查看这里 -> [https://gitee.com/markerhub/markerhub](https://gitee.com/markerhub/markerhub)
# 更多项目学习，请查看这里 -> [https://gitee.com/markerhub/markerhub](https://gitee.com/markerhub/markerhub)

---

我们的开源博客项目eblog已经更新完毕了。今天，我们使用docker的安装方式，来安装一下我们的项目，实验环境是centos 7系统上，本实验适用部署大部分Springboot项目。

eblog项目地址：

[https://gitee.com/MarkerHub/eblog](https://gitee.com/MarkerHub/eblog)

### 安装docker

```
#安装
yum install docker

#检验安装是否成功
[root@localhost opt]# docker --version
Docker version 1.13.1, build 7f2769b/1.13.1

#启动
systemctl start docker

#换镜像源
sudo vim /etc/docker/daemon.json
内容如下：
{
 "registry-mirrors": ["https://m9r2r2uj.mirror.aliyuncs.com"]
}
保存退出，重启docker

#重启
systemctl restart docker
```
### 安装redis

首先上dockerHub搜索redis，点击进入详情页之后，拉到下面就可以看到how to use，如果需要选择特定的版本，有Supported tags给我们选择，然后如果拉取最新的版本的话，拉倒下面就教程。

* [https://hub.docker.com/_/redis](https://hub.docker.com/_/redis)
```
#拉取redis的镜像
docker pull redis
#查看本地redis镜像
docker images

#运行redis
docker run --name myredis -p 6379:6379 -d redis redis-server --appendonly yes
```
* docker run表示运行的意思
* --name myredis 表示起个名字叫myredis
* -p 6379:6379表示把服务器的6379映射到docker的6379端口，这样就可以通过服务器的端口访问docker的端口
* -d 表示以后台服务形式运行redis
* redis redis-server --appendonly yes表示开启持久化缓存模式，可以存到硬盘
### 安装mysql

* [https://hub.docker.com/_/mysql](https://hub.docker.com/_/mysql)
  MYSQL_ROOT_PASSWORD=admin表示root的初始密码

mysql:5.7.27表示操作的是mysql的5.7.27版本，没有后面的版本号的话，默认是拉取最新版本的mysql。

```
docker pull mysql:5.7.27

docker run --name mymysql -e MYSQL_ROOT_PASSWORD=admin -d -p 3306:3306  mysql:5.7.27 
```
连上mysql，创建数据库eblog，然后把数据库脚本导入进去。
脚本位置：[https://github.com/MarkerHub/eblog/blob/master/eblog.sql](https://github.com/MarkerHub/eblog/blob/master/eblog.sql)

### 安装RabbitMq

一行命令搞定，注意RABBITMQ_DEFAULT_PASS=password是设置密码的意思哈。

```
docker run -d --hostname my-rabbit --name myrabbit -e RABBITMQ_DEFAULT_USER=root -e RABBITMQ_DEFAULT_PASS=admin -p 15672:15672 -p 5672:5672 rabbitmq:management
```
### 安装ElasticSearch

docker 安装 Elasticsearch6.4.3版本 及中文插件安装。

系统配置

不配置的话，可能会启动失败

具体报错：max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]。

解决：

* sudo sysctl -w vm.max_map_count=262144
  启动 ES

* docker run -p 9200:9200 -p 9300:9300 -d --name es_643 elasticsearch:6.4.3
  进入镜像

* docker exec -it es_643 /bin/bash
  es配置文件位置： /usr/share/elasticsearch/config/elasticsearch.yml

安装中文分词插件

```
./bin/elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v6.4.3/elasticsearch-analysis-ik-6.4.3.zip
```
退出并重启镜像
* exit
* docker restart es_643
### 构建eblog的docker镜像

接下来，我们需要先去clone eblog博客项目，对项目进行打包成jar包。

```
clone https://github.com/MarkerHub/eblog.git
cd eblog

# 打包
mvn clean package -Dmaven.test.skip=true
```
然后把项目 eblog-0.0.1-SNAPSHOT.jar 包上传到服务器中
同eblog-0.0.1-SNAPSHOT.jar的目录，创建一个名称为Dockerfile文件。

内容如下：

* Dockerfile
```
FROM java:8
EXPOSE 8080

VOLUME /tmp

ENV TZ=Asia/Shanghai
RUN ln -sf /usr/share/zoneinfo/{TZ} /etc/localtime && echo "{TZ}" > /etc/timezone

ADD eblog-0.0.1-SNAPSHOT.jar  /app.jar
RUN bash -c 'touch /app.jar'
ENTRYPOINT ["java","-jar","/app.jar"]
```
* FROM java:8 表示基于jdk8环境
* EXPOSE 8080  表示对外暴露的端口是8080
* * VOLUME /tmp 表示挂载到/tmp目录
* ADD eblog-0.0.1-SNAPSHOT.jar /app.jar 表示把jar包复制到镜像服务里面的根目录，并改名称app.jar
* RUN bash -c 'touch /app.jar' 表示执行创建app.jar
* ENTRYPOINT ["java","-jar","/app.jar"] 表示执行启动命令java -jar
  接下来，我们安装Dockrfile的命令，把eblog-0.0.1-SNAPSHOT.jar构建成docker的镜像。

```
#构建镜像，注意后面有个点哈。
docker build -t eblog .

#查看镜像
docker images
```
这步骤完成之后，我们就可以在准备工作就已经完成啦，接下来，我们就直接启动我们的项目哈。
### 启动eblog项目

启动命令如下：

```
docker run -p 8080:8080 -p 9326:9326 --name eblog --link es_643:ees --link myrabbit:erabbit --link mymysql:emysql --link myredis:eredis -d eblog
```
-p 8080:8080 -p 9326:9326 ：9326是因为即时聊天需要用到的ws端口
--link es:ees 表示关联容器，把容器es起别名为ees

查看eblog打印日志

```
docker logs -f eblog
```

这时候我们查看就可以通过8080端口访问我们的项目了！！！

到此项目运行成功！！

### 总结

使用docker compose编排形式会更简单！

