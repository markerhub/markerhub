你好，这里是**MarkerHub**，一个**专注于Java项目开发与教学**的学习基地。

这里所有的项目都是从0开发，手把手带敲教学，由我（吕一明）本人**亲自开发和录制**，拥有**完整原创视频、文档、源码**，**版权所有**
，可以用于个人学习与参考！**禁止任何形式的分享和售卖或商业！！**

--

**【免费项目】**

其中综合博客项目是一个难度比较大的学习项目，需要一定基础。其他适合做入门项目学习。

| 项目名称                          | 技术栈                                   |                                                                                                        操作                                                                                                        |
|:------------------------------|:--------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| 1. 【从0开发】博客项目完整教学VueBlog      | SpringBoot2、Shiro、Vue2                |                       [文档](https://gitee.com/markerhub/markerhub/tree/master/项目文档/vueblog)- [视频](https://www.bilibili.com/video/BV1PQ4y1P7hZ) - [源码](https://gitee.com/markerhub/vueblog)                        |
| 2. 【从0开发】后台权限管理系统完整教学VueAdmin | SpringBoot2、SpringSecurity、Vue2       | [预览](https://www.markerhub.com/vueadmin) - [文档](https://gitee.com/markerhub/markerhub/tree/master/项目文档/vueadmin)- [视频](https://www.bilibili.com/video/BV1af4y1s7Wh) - [源码](https://gitee.com/markerhub/VueAdmin) |
| 3. 【从0开发】综合博客传统项目完整教学eblog    | Rabbit、ES、t-io、WebSocket、Freemarker   |                         [文档](https://gitee.com/markerhub/markerhub/tree/master/项目文档/eblog)- [视频](https://www.bilibili.com/video/BV1ri4y1x71A/) - [源码](https://gitee.com/markerhub/eblog)                         |
| 4. 【从0开发】用户中心项目user-center    | JDK21、SpringBoot3、Vue3                |                                                                  [文档](https://gitee.com/markerhub/markerhub/tree/master/user-center)   - [视频]()                                                                  |
| 5. Docker部署传统SpringBoot项目     | SpringBoot2、Docker、Centos7            |                                                 [文档](https://gitee.com/markerhub/markerhub/tree/master/eblog)- [视频](https://www.bilibili.com/video/BV1dk4y1r7pi)                                                 |
| 6. Docker部署SpringBoot+Vue项目   | SpringBoot2、Vue2、Docker Compose、Nginx |                                             [文档](https://gitee.com/markerhub/markerhub/tree/master/项目文档/vueblog)- [视频](https://www.bilibili.com/video/BV17A411E7aE)                                              |

--

**【VueShop综合微商城项目]**

目前基于SpringBoot单体版本和SpringCloud微服务版本的课程视频和文档已录制和编写完结。此项目会继续迭代，还会追加秒杀业务、Dubbo微服务版本、多租户版本等内容。此项目适合有一定开发经验的同学！

| 项目名称                            | 技术栈                        |                                                                                                                                              操作                                                                                                                                               |
|:--------------------------------|:---------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| 1. 【从0开发】VueShop微商城单体版本 -222集   | SpringBoot2、rabbit、es、Vue3 | [预览](https://www.markerhub.com/vueshop)  - [介绍](https://gitee.com/markerhub/markerhub/blob/master/%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D/1%E3%80%81VueShop%E5%BE%AE%E5%95%86%E5%9F%8E%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D.md) - [视频](https://pan.baidu.com/s/1_h1IElb5lus5CmcKb1xxqw?pwd=h6j1)  |
| 2. 【从0开发】VueShop微商城微服务迭代版本 -57集 | SpringCloud、sa-token、Vue3  | [预览](https://www.markerhub.com/vueshop)  - [介绍](https://gitee.com/markerhub/markerhub/blob/master/%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D/1%E3%80%81VueShop%E5%BE%AE%E5%95%86%E5%9F%8E%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D.md)  - [视频](https://pan.baidu.com/s/1_h1IElb5lus5CmcKb1xxqw?pwd=h6j1) |

--

**【已完结BS系列项目】**

此系列项目业务和技术栈都相对简单，适合小白入门和新手学习项目，

| 项目名称                          | 技术栈                            |                                                                                                                                                         操作                                                                                                                                                         |
|:------------------------------|:-------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| 1. 【从0开发】收藏检索系统DailyHub       | SpringBoot2、canal、es、Bootstrap |                      [介绍](https://gitee.com/markerhub/markerhub/blob/master/%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D/2%E3%80%81%E6%94%B6%E8%97%8F%E6%A3%80%E7%B4%A2%E7%BD%91%E7%AB%99%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D.md)   - [视频](https://pan.baidu.com/s/1qcsPOaPkkOCBjCasFCstew?pwd=dwwh)                       |
| 2. 【从0开发】美食菜谱分享平台+推荐算法bs-food | SpringBoot3、协同推荐、Vue3          | [预览](https://www.markerhub.com/bs-food)  - [介绍](https://gitee.com/markerhub/markerhub/blob/master/%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D/3%E3%80%81%E7%BE%8E%E9%A3%9F%E5%88%86%E4%BA%AB%E5%B9%B3%E5%8F%B0%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D.md)   - [视频](https://pan.baidu.com/s/1KS4xFsla5bNQstQAoMCWYA?pwd=296u) |
| 3. 【从0开发】宠物领养救助平台bs-pets      | SpringBoot3、Vue3               | [预览](https://www.markerhub.com/bs-pets)  - [介绍](https://gitee.com/markerhub/markerhub/blob/master/%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D/4%E3%80%81%E5%AE%A0%E7%89%A9%E9%A2%86%E5%85%BB%E5%B9%B3%E5%8F%B0%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D.md)  - [视频](https://pan.baidu.com/s/1KS4xFsla5bNQstQAoMCWYA?pwd=296u)  |

--

**【准备中BS系列项目】** - 每个月会录制3个左右项目

1. 【从0开发】新闻宣传网站
2. 【从0开发】鲜花销售网站
3. 【从0开发】二手交易平台
4. 【从0开发】博客论坛网站
5. 【从0开发】电影票购买网站
6. 【从0开发】校园跑腿小程序
7. 【从0开发】外卖点餐小程序
8. 【从0开发】驾校报名预约系统
9. 【从0开发】在线答题系统
10. 【从0开发】小说阅读小程序
11. 【从0开发】酒店预订系统
12. 【从0开发】员工业绩考核系统
13. 【从0开发】汽车销售网站
14. 【从0开发】房屋租赁管理系统
15. 【从0开发】民宿在线预订系统
16. 【从0开发】智慧社区系统
17. 【从0开发】图书管理系统
18. 【从0开发】校园资料分享平台
19. 【从0开发】母婴商城网站
20. 【从0开发】在线拍卖系统
21. 【从0开发】在线网盘系统
22. 【从0开发】音乐播放平台
23. 【从0开发】宿舍管理系统
24. 【从0开发】失物招领平台

正在火速更新中...

--

所有项目都是我自己一个人完成、项目拥有完整项目讲解文档、视频、源码等资料。

课程费用： **118元**，包含：【VueShop系列】【BS系列】所有项目课程。

**如果您有需要购买或咨询，请加入我微信 javamind**，并备注【项目学习】

**_注意：：_**_付费后，每人仅可以免费获取一份项目源码，但可以学习所有项目（文档、视频）_！

![加微信javamind](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/mine/javamind-qr.png)

提供微信项目答疑群， 所有资料都会在微信群里第一时间提供和通知！

项目上的学习问题，也可以提起 Issues，我会第一时间回答你！

**常见疑问与回答
**：[请点击这里查看](https://gitee.com/markerhub/markerhub/blob/master/%E9%A1%B9%E7%9B%AE%E4%BB%8B%E7%BB%8D/0%E3%80%81%E5%B8%B8%E8%A7%81%E7%96%91%E9%97%AE%E4%B8%8E%E8%A7%A3%E7%AD%94.md)
