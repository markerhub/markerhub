### 项目简介

这是一个基于springboot、spring cloud的前后端分离的微商城项目，包括手机端微商城项目和后台管理系统，整个电商购物流程已经能流畅支持，涵盖商品浏览、搜索、商品评论、商品规格选择、加入购物车、立即购买、下单、订单支付、后台发货、退货等。功能强大，主流技术栈，非常值得学习。

项目包含2个版本：

* 基于springboot的单体版本
* 基于spring cloud alibaba的微服务版本

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/c136c97dbfcd4646aecd383ac349149e.png)

### 技术栈

单体版本：springboot 2.7、mybatis plus、rabbitmq、elasticsearch、redis

微服务版本：spring cloud alibaba 2021.0.5.0，nacos、seata、openFeign、sentinel

前端：vue 3.2、element plus、vant ui、

### 项目截图

线上演示：[https://www.markerhub.com/vueshop](https://www.markerhub.com/vueshop)

**后台展示**

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/93ead20c42a0409182ff5f2b0013e82b.png)

**微商城展示**

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/70498eb03cac47de94c85073f8960732.png)

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/e3a56cdb807a476896e8eeb3275b1940.png)

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/df57e64ceec343a08ff54885a1a67601.png)

### 视频与文档

**单体版本文档讲解：**

#### ![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/dd8071def23b4697a081c143eafe8dbe.png)

**单体版本视频讲解（****222集）****：**

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/d096ec26953c4ff9b5a15beaee2c617d.png)

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/b30c790e13a64a878186a5e24ed0a929.png)

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/a753922961384da8883feea6c4630ca5.png)

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/932a450620154c95aef5838310d323ca.png)

**微服务版本文档讲解**

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/c6a6bfb58fd242d8877c002a4c1fa1df.png)

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/b7480d5816fe45f3a9cbbb62633c23df.png)

**微服务迭代视频讲解（57集）**

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/df35424982bb4f6ab7a20e6faa2d524b.png)

**接口调试**

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/4ddc9997d4574ab1819dc85019bf2b59.png)

### 教程内容

整套微商城项目的学习资源是已经非常完善，从文档到视频、接口调试、学习看板等方面，让项目学习更加容易，内容更加沉淀。全套视频教程约48小时，共279期，讲解非常详细细腻。下面详细为大家介绍：

#### 架构篇

* ***单体版本***

使用主流的技术架构，真正手把手教你从0到1如何搭建项目手脚架、项目架构分析、建表逻辑、业务分析、实现等。涵盖SpringBoot、Mybatis Plus、Jwt、Redis、Lombok、Hutool、Shiro、Elasticsearch、RabbitMq、Docker、等技术。

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/00bd4d21b52d47d88d82c9a450440f3c.png)

* ***微服务版本***

使用基于sprng cloud alibaba的微服务技术架构体系，涵盖SpringCloud Alibaba、Sa-Token、Nacos、Seata、OpenFeign、Loadbancencer、Redis、Elasticsearch、RabbitMq、Docker等主流技术体系。

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/fc552b9cdb9d41d0bfb782ccced2c407.png)

#### 业务篇

vueshop微商城的整个购物流程已经完善，各个模块的业务都是已经实现，涵盖商品模块、搜索模块、购物车模块、订单模块、退款模块、后台权限模块、业务数据管理模块等

![图片](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240524/3f4868589a8d4e7e86351f4c3533d527.png)

### 简历模板

![](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240726/90c57f7dd9064bafb53149fcd1cd8ddd.png)
![](https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/20240726/72dcbf3cb7ed4efd8e78f17280015529.png)


### 学习与收货

这是一个完善的微商城项目，非常适合新手以及初中高级程序员，如果你正想学习一个大型的Java开发项目，这是你非常明智的选择，让你快速拥有一个完整的项目经验，可写入面试简历、毕业设计等。

- 本教程包含项目的功能设计、数据库设计、业务解决方案、接口设计思路等，可提高项目开发能力与效率
- 项目从0到1开发，真正手把手教学，可获得项目完整开发经验
- 作为一个微商城项目，里面设计很多经典的业务问题，比如超买超卖、幂等防重设计、定时取消支付订单、支付对接等问题，可提高眼界与开拓思路
- 都是用主流的技术栈开发，可作为毕业设计与面试项目。
- 包含单体版本与微服务版本，学习项目架构迭代过程

### 视频试看

https://www.bilibili.com/video/BV1wb4y1T76G

### 如何获取与收费

目前全套视频和文档、源码等资料已经录制或编写完毕，并已经上传到网盘，这套微商城项目代码开发、文档编写、视频录制与剪辑花费了我前前后后差不多5个月时间，所以想收费收回点时间成本费。

目前在优惠推广期间，价格定在 **118元**，如果你感兴趣，可以加我微信，备注一下【项目学习】，成交后会发项目所有资源给你。感谢支持！
